import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    titles = ["Mr.", "Mrs.", "Miss."]
    result = []

    for title in titles:
        group = data[data['Name'].str.contains(title)]
    
        missing_values = group['Age'].isnull().sum()
    
        median_value = round(group['Age'].median(skipna=True))
    
        result.append((title, missing_values, median_value))

    return result
